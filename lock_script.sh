#!/bin/bash
scr='/home/anmol/.config/i3/scr.png'
gradientColor='#2B2724'
gradientDimensions='1366x768'
scrot "$scr"
convert "$scr" -blur 0x5 -size "$gradientDimensions" -gravity south-west gradient:none-"$gradientColor" -composite "$scr"
i3lock -ui "$scr"
rm $scr
