import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import requests, json, urllib, threading
from PIL import Image

def json_resp(url):
	r = requests.get(url)
	data = json.loads(r.text)
	return data

def get_latest_num():
	return str(json_resp("https://xkcd.com/info.0.json")['num'])

class XkcdViewer(Gtk.Window):
	def __init__(self, title):
		Gtk.Window.__init__(self, title="xkcd Viewer")
		self.set_border_width(20)
		self.main_box = Gtk.Box(
			orientation=Gtk.Orientation.VERTICAL, spacing=6)
		self.set_position(Gtk.WindowPosition.CENTER)
		self.connect("destroy",Gtk.main_quit)
		self.set_default_size(800, 700)
		self.num = ""
		self.flag = 0
		self.parent_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
		
		self.latest = get_latest_num()
		self.win = Gtk.Window()
		tbox = self.create_input_box()
		self.parent_box.pack_start(tbox, False, False, 10)

		comic_vbox = self.create_comic_box()
		main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
		main_box.pack_start(comic_vbox, True, True, 0)
		self.get_comic()
		
		child_scrolled = Gtk.ScrolledWindow()
		child_scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		child_scrolled.add(main_box)
		
		self.parent_box.pack_start(child_scrolled, True, True, 0)
		self.parent_box.connect('key-press-event', self.on_nav)
		scrolled = Gtk.ScrolledWindow()
		scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
		scrolled.add(self.parent_box)
		self.add(scrolled)
		self.show_all()

	def on_nav(self, widget, ev, data=None):
		if ev.keyval == Gdk.KEY_Right and self.num < self.latest:
			self.num = str(int(self.num) + 1)
			self.flag = 1
			self.get_comic()
		elif ev.keyval == Gdk.KEY_Left and int(self.num) > 1:
			self.num = str(int(self.num) - 1)
			self.flag = 1
			self.get_comic()

	def on_key_release(self, widget, ev, data=None):
		if ev.keyval == Gdk.KEY_Return:
			self.get_comic()

	def create_comic_box(self):
		comic_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
		self.title = Gtk.Label()
		self.title.set_justify(Gtk.Justification.CENTER)

		self.spinner = Gtk.Spinner()
		
		self.img = Gtk.Image()
		self.img.set_property("margin_left", 40)
		self.img.set_property("margin_right", 40)

		comic_vbox.pack_start(self.title, False, False, 5)
		comic_vbox.pack_start(self.spinner, False, False, 5)
		comic_vbox.pack_start(self.img, False, False, 5)
		return comic_vbox

	def fetcher(self, url, path):
		img_url = json_resp(url)['img']
		try:
			urllib.urlretrieve(img_url, path)
			self.title.set_text('xkcd #' + self.num)
			self.img.set_from_file(path)
			self.img.show()
			self.spinner.stop()
		except:
			print('here')

	def get_comic(self, *args):
		if(self.flag == 0):
			try:
				self.num = str(int(self.input.get_text()))
			except:
				pass
			self.input.set_text("");
			if(self.num == ""):
					self.num = self.latest
		else:
			self.flag = 0
		if(int(self.num) <= 0 or int(self.num) > int(self.latest)):
			return
		self.spinner.start()
		path = '/tmp/' + self.num
		url = "https://xkcd.com/" + self.num + "/info.0.json"

		thread = threading.Thread(target=self.fetcher, kwargs={'url': url, 'path': path})
		thread.daemon = True
		thread.start()

	def create_input_box(self):
		entry_hbox = Gtk.Box(
			orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
		entry_hbox.set_property("margin", 10)

		self.input = Gtk.Entry()
		self.input.connect("key-release-event", self.on_key_release)
		entry_hbox.pack_start(self.input, True, True, 0)

		submit = Gtk.Button.new_with_label("Go!")
		submit.connect("clicked", self.get_comic)
		entry_hbox.pack_start(submit, True, True, 0)

		return entry_hbox

x = XkcdViewer("xkcd Viewer")
Gtk.main()