set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec --no-startup-id i3-sensible-terminal
bindsym $mod+t exec --no-startup-id xfce4-terminal

# fetch
bindsym $mod+Shift+Return exec --no-startup-id urxvt -name 'fetch' -e bash -c "sleep 0.1 && neofetch --backend w3m --source 'wallpaper' --crop_mode 'normal' && bash"
for_window [class="URxvt" instance="fetch"] floating enable
for_window [class="mpv"] floating enable
for_window [class=Polybar] floating enable
for_window [class="Wicd-client.py"] floating enable
for_window [class="Baobab"] floating enable
for_window [class="Lxappearance"] floating enable  

# kill focused window
bindsym $mod+Shift+q kill
bindsym Mod1+F4 kill

# rofi
bindsym $mod+d exec rofi -show run -lines 4 -eh 2.5 -width 50 -padding 30 -font "ubuntu light 17"

# Constants
set $mod Mod4
set $workspace1 1:  
set $workspace2 2:  
set $workspace3 3:  
set $workspace4 4:  
set $workspace5 5:  
set $workspace6 6: 
set $workspace7 7
set $workspace8 8
set $workspace9 9
set $workspace10 10
  
# Start applications
bindsym Shift+Print exec --no-startup-id scrot '%Y-%m-%d-%H-%M-%S_scrot.png' -q 100
bindsym $mod+Shift+f exec --no-startup-id chromium
bindsym $mod+Shift+t exec --no-startup-id thunar
bindsym $mod+Shift+n exec --no-startup-id urxvt -e ncmpcpp
bindsym $mod+Shift+m exec --no-startup-id "~/.config/i3/./play_music.sh"
#bindsym $mod+Shift+x exec --no-startup-id ~/.config/i3/lock_script.sh
bindsym $mod+Shift+x exec --no-startup-id i3lock-fancy -pf Ubuntu -- scrot -z
bindsym $mod+Shift+w exec --no-startup-id ~/.config/i3/wall_script.sh
bindsym $mod+Shift+b exec --no-startup-id bleachbit
bindsym $mod+Shift+s exec --no-startup-id subl3
bindsym $mod+Mod1+s exec --no-startup-id "spotify --force-device-scale-factor=1.000001"

# Program Workspaces
assign [class="Chromium"] $workspace2
assign [class="Thunar"] $workspace3
assign [class="factorio"] $workspace6
assign [class="SpeedRunners.bin.x86_64"] $workspace6
for_window [class="Spotify"] move to workspace $workspace5

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle
bindsym $mod+Mod1+space sticky toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent
#Rebinding to rofi application mode
bindsym $mod+a exec rofi -show drun -lines 4 -eh 2.5 -width 50 -padding 30 -font "ubuntu light 17"

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+9 workspace $workspace9
bindsym $mod+0 workspace $workspace10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9
bindsym $mod+Shift+0 move container to workspace $workspace10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
bindsym $mod+Shift+Mod1+r exec --no-startup-id polybar-msg cmd restart

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
set $bg-color          #00000000
set $inactive-bg-color   #00000000
set $text-color          #F3F4F5
set $inactive-text-color #7B8DB3
set $urgent-text-color   #FC524F
#bar{
	#i3bar_command i3bar -t
	#status_command python ~/.config/i3/pystatus.py
    #tray_output none
	#height 20
	#position top
	#status_command i3status -c ~/.config/i3/i3status.conf
	#font pango: Ubuntu Light 12
	#colors {
	#	background $bg-color
	#    separator #757575
	#	#                  border             background         text
	#	focused_workspace  $bg-color          $bg-color          $text-color
	#	inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
	#	urgent_workspace   $bg-color   		  $bg-color		     $urgent-text-color
    #}
#}

# Gaps
gaps inner 15
gaps outer 5
#smart_gaps on

# Colors
set $FOCUS #F9F7F3
set $UNFOCUS #59545D
set $BORDERF #F9F7F3
set $BORDERU #59545D
client.focused 		  	$color12    $BORDERF	$FOCUS		$BORDERF
client.focused_inactive		$color12    $UNFOCUS	$UNFOCUS	$UNFOCUS
client.unfocused		$color4     $BORDERU	$UNFOCUS	$BORDERU
client.urgent			$color5     $FOCUS	$FOCUS		$FOCUS
client.background		$UNFOCUS

# Border size
for_window [class="^.*"] border pixel 3
for_window [class="Tk"] border pixel 0
for_window [class="mpv"] border pixel 0
for_window [class="Polybar"] border pixel 0
for_window [class="Tk"] sticky enable

#Scartchpad
bindsym $mod+minus scratchpad show
bindsym $mod+Shift+minus move scratchpad

# Brightness control
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 10
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10

# Start-up shit
exec --no-startup-id compton -f --config ~/.config/i3/compton.conf
exec_always --no-startup-id feh --bg-scale ~/Pictures/Walls/2.jpg
exec --no-startup-id python ~/.config/i3/nosleep.py
exec --no-startup-id dunst
exec --no-startup-id xflux -l 28.6 -g 77.2
exec --no-startup-id xrdb .Xresources && polybar -r -c /home/anmol/.config/polybar/config burs
#exec --no-startup-id conky

# Volume control
bindsym XF86AudioLowerVolume exec --no-startup-id amixer set Master 3%-
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer set Master 3%+
bindsym XF86AudioMute exec --no-startup-id amixer set Master toggle

# Media controls
bindsym XF86AudioPlay exec --no-startup-id "playerctl play-pause;mpc toggle;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
bindsym XF86AudioNext exec --no-startup-id "playerctl next; mpc next;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"
bindsym XF86AudioPrev exec --no-startup-id "playerctl previous;mpc prev;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"

# Mouse startup
exec --no-startup-id xinput set-prop "SynPS/2 Synaptics TouchPad" "Synaptics Scrolling Distance" -114, -114
exec --no-startup-id xinput set-prop "SynPS/2 Synaptics TouchPad" "Synaptics Two-Finger Scrolling" 1, 1
exec --no-startup-id xinput set-prop "SynPS/2 Synaptics TouchPad" "Synaptics Tap Action" 0, 0, 0, 0, 1, 3, 2
exec --no-startup-id xinput set-prop "SynPS/2 Synaptics TouchPad" "Device Accel Constant Deceleration" 3.5

# Tearing fix
new_window 1pixel

bindsym Mod1+Shift+F4 exec --no-startup-id oblogout
for_window [class="Oblogout"] fullscreen enable
for_window [class="factorio"] fullscreen enable
for_window [class="SpeedRunners.bin.x86_64"] fullscreen enable
