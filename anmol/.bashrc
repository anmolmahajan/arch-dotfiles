#
# ~/.bashrc
#

if [[ -f /etc/bash_completion ]]; then
	. /etc/bash_completion
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='┌─[\[\e[1;34m\]\#\[\e[00m\]][\[\e[01;32m\]\u\[\e[00m\]@\[\e[01;32m\]\h\[\e[00m\]:\[\e[1;34m\]\w\[\e[0m\]]\n└──╼ '

alias gitbit='git add .;git commit -m "$(date)";git push -u origin master'
alias spotify='spotify --force-device-scale-factor=1.000001'
alias neofetch="neofetch --backend w3m --source 'wallpaper' --crop_mode 'normal'"
alias aria3c='aria2c --max-connection-per-server=16 --min-split-size=1M --split=16'
alias kaboom='sudo pacman -Syyu && yaourt -Syua'
