inoremap jk <ESC>
set background=dark
colorscheme base16-railscasts


filetype plugin indent on
execute pathogen#infect()
set number
set numberwidth=1
syntax on
set encoding=utf-8
  highlight clear SignColumn
  highlight VertSplit    ctermbg=236
  highlight ColorColumn  ctermbg=237
  highlight LineNr       ctermbg=236 ctermfg=240
  highlight CursorLineNr ctermbg=236 ctermfg=240
  highlight CursorLine   ctermbg=236
  highlight StatusLineNC   ctermfg=255 ctermbg=240
  highlight StatusLine ctermfg=240 ctermbg=255
  highlight IncSearch    ctermbg=3   ctermfg=1
  highlight Search       ctermbg=1   ctermfg=3
  highlight Visual       ctermbg=3   ctermfg=0
  highlight Pmenu        ctermbg=240 ctermfg=12
  highlight PmenuSel     ctermbg=3   ctermfg=1
  highlight SpellBad     ctermbg=0   ctermfg=1

"NERDTree stuff
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
let g:NERDTreeWinSize=20

function! NERDTreeQuit()
  redir => buffersoutput
  silent buffers
  redir END
"                     1BufNo  2Mods.     3File           4LineNo
  let pattern = '^\s*\(\d\+\)\(.....\) "\(.*\)"\s\+line \(\d\+\)$'
  let windowfound = 0

  for bline in split(buffersoutput, "\n")
    let m = matchlist(bline, pattern)

    if (len(m) > 0)
      if (m[2] =~ '..a..')
        let windowfound = 1
      endif
    endif
  endfor

  if (!windowfound)
    quitall
  endif
endfunction
autocmd WinEnter * call NERDTreeQuit()
nnoremap <F4> :NERDTreeToggle<CR>



set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = '/usr/bin/python2'


 set statusline=%t       "tail of the filename
 set statusline+=%y      "filetype
 set statusline+=%c,     "cursor column
 set statusline+=%l/%L   "cursor line/total lines
 set statusline+=\ %P    "percent through file
 
 "Cursor shapes according to modes
 let &t_SI = "\<Esc>[5 q"
 let &t_SR = "\<Esc>[3 q"
 let &t_EI = "\<Esc>[1 q"


"Tab Bindings
 nnoremap <F2> :tabprevious<CR>
 nnoremap <F3> :tabnext<CR>
