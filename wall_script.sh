#!/bin/bash
cd ~/.config/i3
n="$(cat config | grep feh | grep -Eo '[0-9]{1,4}')"
prev=$n
let n=$((n + 1))
cd ~/Pictures/Walls
if [ "$n" -le "$(ls -1 | wc -l)" ]
then
	next=$n	
#sed -i 's/wallpaper.jpg/wallpaper.png/g' config
#feh --bg-scale ~/.i3/wallpaper.png
else
	next="1"
#sed -i 's/wallpaper.png/wallpaper.jpg/g' config
#feh --bg-scale ~/.i3/wallpaper.jpg
fi
prev="$prev.jpg"
next="$next.jpg"
cd ~/.config/i3
sed -i "s@$prev@$next@g" config
cd ~/.config/bspwm/
sed -i "s@$prev@$next@g" bspwmrc
wal="$HOME/Pictures/Walls/$next" 
feh --bg-scale $wal


