#!/bin/sh
num=1
pref=$1
for file in *.jpg; do
       mv "$file" "$pref$(printf "%u" $num).jpg"
       let num=$num+1
done
for file in *.png; do
       mv "$file" "$pref$(printf "%u" $num).png"
       let num=$num+1
done
