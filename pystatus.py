# -*- coding: utf-8 -*-
from i3pystatus import Status

status = Status(standalone=True)

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
#                          ^-- calendar week
status.register("clock",
    format=" %-d %a %H:%M",
    color="#b1c4c0",
    interval=60,
    on_doubleleftclick="urxvt -name 'fetch' -e bash -c \"neofetch --backend w3m --source 'wallpaper' --crop_mode 'normal' && bash\"")

# The battery monitor has many formatting options, see README for details

# This would look like this, when discharging (or charging)
# ↓14.22W 56.15% [77.81%] 2h:41m
# And like this if full:
# =14.22W 100.0% [91.21%]
#
# This would also display a desktop notification (via D-Bus) if the percentage
# goes below 5 percent while discharging. The block will also color RED.
# If you don't have a desktop notification demon yet, take a look at dunst:
#   http://www.knopwob.org/dunst/
status.register("battery",
    format="{status} {percentage:.2f}% - {remaining:%E%hh:%Mm}",
    alert=False,
    alert_percentage=10,
    color="#DFA719",
    charging_color="#BBCDF3",
    full_color="#5AD1C8",
    critical_color="#FF5552",
    status={
        "DIS": "",
        "CHR": "",
        "FULL": "",
    },
    on_doubleleftclick="urxvt -name 'fetch' -e bash -c \"sudo powertop\"",
#    hints = {"separator": False},
    )


# Shows the average load of the last minute and the last 5 minutes
# (the default value for format is used)
status.register("load",
    format=" {avg1}",
    color="#24B34E",
    critical_color="#D10F27",
    critical_limit=2,
    hints = {"separator": False},
    )

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp",
    format=" {temp:.0f}°C",
    color="#FC524F",
    hints = {"separator": False},
    )





# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces


# Note: requires both netifaces and basiciw (for essid and quality)
status.register("network",
    interface="wlp8s0",
    format_up=" {quality:03.0f}% @ {essid}",
    format_down="Not Connected @ wlp8s0",
    color_up="#30AF85",
    color_down="#FC524F",
    on_doubleleftclick="wicd-gtk",
    on_rightclick="urxvt -name 'fetch' -e bash -c \"ping -c 4 www.google.com\""
#    hints = {"separator": False},
    )

status.register("mem",
    format=" {used_mem}/{total_mem} MiB",
    color="#44B3A4",
    on_doubleleftclick="urxvt -name 'fetch' -e bash -c \"htop\"",
#    hints = {"separator": False},
    )

# Shows disk usage of /
# Format:
# 42/128G [86G]
status.register("disk",
    path="/",
    format=" {avail}/{total} G",
    color="#46ABF2",
    hints = {"separator": False},
    on_doubleleftclick="baobab",
    )

# Shows pulseaudio default sink volume
#
# Note: requires libpulseaudio from PyPI
status.register("alsa",
    format=" {volume}%",
    format_muted="  {volume}%",
    on_leftclick=["switch_mute"],
    on_upscroll=["increase_volume"],
    on_downscroll=["decrease_volume"],
    on_rightclick=["run"],
    color="#4AC1B8",
    color_muted="#DC322F",
    on_doubleleftclick="urxvt -name 'fetch' -e alsamixer",
#    hints = {"separator": False},
)


# Shows mpd status
# Format:
# Cloud connected▶Reroute to Remain
status.register("mpd",
    format="{status} {title} ({song_elapsed}/{song_length})",
    status={
        "pause": "",
        "play": "",
        "stop": "",
    },
    color="#298D9C",
    on_leftclick=["switch_playpause"],
    on_rightclick=["next_song"],
    on_doublerightclick=["previous_song"],
    on_doubleleftclick="urxvt -e ncmpcpp",
    on_upscroll=None,
    on_downscroll=None,
    hide_inactive=True,
    interval=1,
    hints = {"separator": False},
    )




status.run()